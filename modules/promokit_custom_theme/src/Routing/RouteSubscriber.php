<?php
/**
 * @file
 * Contains \Drupal\promokit_custom_theme\Routing\RouteSubscriber.
 */
namespace Drupal\promokit_custom_theme\Routing;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {
  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route) {
      // First simple approach.
      if ($route->getPath() == '/presentation') {
        $route->setOption('_custom_theme', 'promokit_presentation');
      }
      if ($route->getPath() == '/landing') {
        $route->setOption('_custom_theme', 'promokit_landing');
      }
    }

  }
}
