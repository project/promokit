/**
 * @file
 * Adds edit link to the slides.
 */
(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Returns an ID of active node slide.
   * @returns {*}
   */
  var getActiveNodeID = function() {

    var slide = $('.slide.active');

    // In the beginning no one slide is active so we use the first inactive.
    if (slide.length == 0) {
      slide = $('.slide');
    }

    // If there is not any slides.
    if (slide.length == 0) {
      return '';
    }
    // If more than one slide have been returned.
    else if (slide.length > 1) {
      slide = slide.first();
    }

    // Parse ID attribute to find node.nid.
    return slide.prop('id').split('-')[1];
  };

  /**
   * Appends edit link to the node body.
   */
  var renderEditLink = function() {
    var node_id = getActiveNodeID();

    // @todo: Find a way to use drupalSettings.user.uid instead. It's not passed for anonymous.
    var path = drupalSettings.path.baseUrl + 'dashboard/' + node_id;
    if (drupalSettings.anonymous) {
      path = drupalSettings.path.baseUrl + 'user/login?destination=dashboard/' + node_id;
    }

    // Replace edit link target with relevant path if the link exists.
    if ($('.promokit-presentation-edit-btn').length > 0) {
      $('.promokit-presentation-edit-btn > a').attr('href', path);
      return;
    }

    // Add edit link.
    $('body').append('<div class="promokit-presentation-edit-btn"><a href="' + path + '">' + Drupal.t('edit') + '</a></div>');
  };

  /**
   * Adds edit button to the bottom of the slides to be able to go back to the dashboard at any time.
   * @type {{attach: Function}}
   */
  Drupal.behaviors.promokitPresentationEditButton = {
    attach: function (context, settings) {

      renderEditLink();

      // Rerender the edit link when key pressed to always have link to the relevant slide in the dashboard.
      $(document).keydown(function(e) {
        renderEditLink();
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
