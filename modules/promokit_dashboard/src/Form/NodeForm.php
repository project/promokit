<?php


/**
 * @file
 * Custom slide form based on Drupal\node\NodeForm.
 */

namespace Drupal\promokit_dashboard\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class NodeForm extends \Drupal\node\NodeForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var $node \Drupal\node\NodeInterface */
    $node = $this->entity;

    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = array(
      '#type' => 'hidden',
      '#default_value' => $node->getChangedTime(),
    );

    $form = ContentEntityForm::form($form, $form_state);

    // Alter default form.
    $form['title']['widget'][0]['value']['#required'] = FALSE;

    /** @var $url \Drupal\Core\Url */
    $url = Url::fromRoute('<front>', array(), array('fragment' => 'slide-' . $node->id()));
    if(!$node->isNew()) {
      // @todo: Add better styles for the button position.
      $form['title']['widget']['#suffix'] =
        format_string('<a href="!url" class="button button-slide-preview">Preview</a>', array('!url' => $url->toString()));
    }

    $form['field_image']['widget'][0]['#description'] = '';
    $form['body']['widget']['#pre_render'][] = array($this, 'field_body_pre_render');

    $form['#validate'][] = '::validateAddMenu';
    return $form;
  }

  public function validateAddMenu($form, FormStateInterface $form_state) {
    $title = $form_state->getValue(array('title', 0, 'value'));
    if (empty($title)) {
      return;
    }
    $definition = $form_state->getValue('menu');
    $definition['enabled'] = 1;
    $definition['title'] = $title;

    $form_state->setValue('menu', $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function processForm($element, FormStateInterface $form_state, $form) {
    // Hide unnecessary elements.
    foreach (array('menu', 'meta', 'revision_log', 'uid', 'created', 'promote', 'sticky') as $name) {
      if (isset($element[$name])) {
        $element[$name]['#access'] = FALSE;
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = ContentEntityForm::actions($form, $form_state);

    $element['delete']['#access'] = $this->entity->access('delete');
    $element['delete']['#weight'] = 100;

    /* @var $delete_url \Drupal\Core\Url */
    if (isset($element['delete']['#url']) && $delete_url = $element['delete']['#url']) {
      $delete_url->setOption('query', array('destination' => 'dashboard'));
    }

    return $element;
  }

  public function field_body_pre_render($element) {
    $element[0]['format']['guidelines']['#access'] = FALSE;
    $element[0]['format']['format']['#access'] = FALSE;
    $element[0]['format']['help']['#access'] = FALSE;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /* @var $node \Drupal\node\NodeInterface */
    $node = $this->entity;
    $is_new = $node->isNew();

    // Fix slide order value for new nodes.
    if ($is_new) {
      $node->field_weight->value = $this->getMaxSlideOrderValue() + 1;

      // Set menu item weight according to slide order.
      $node->menu['weight'] =  $node->field_weight->value;
    }

    parent::save($form, $form_state);
    drupal_get_messages('status');

    if (!$node->id()) {
      // Something went wrong.
      return;
    }

    // Set normal front page after at least one node was created.
    $front_page = \Drupal::config('system.site')->get('page.front');
    if ($front_page == '/dashboard') {
      \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/presentation')->save();
    }

    if ($is_new) {
      $form_state->setRedirect('promokit.dashboard');
    }
    else {
      $form_state->setRedirect(
        'promokit.dashboard',
        array('slide_id' => $node->id())
      );
    }
  }

  /**
   * Get max slide order value.
   * @return mixed
   */
  private function getMaxSlideOrderValue() {
    return db_query('SELECT MAX(field_weight_value) FROM {node_revision__field_weight}')->fetchField();
  }
}
