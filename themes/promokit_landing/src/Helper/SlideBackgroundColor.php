<?php
/**
 * @file
 * Class for slide background color manipulations.
 */

namespace Drupal\promokit_landing\Helper;

class SlideBackgroundColor {

  private static $backgroundColorClasses = array(
    'primary-color',
    'secondary-color',
    'tertiary-color'
  );

  /**
   * Returns CSS color class for the slide background.
   *
   * @param $slideOrder
   * @return mixed
   */
  public static function GetClass($slideOrder) {
    $modulo = $slideOrder % 3;
    return self::$backgroundColorClasses[$modulo];
  }
}
