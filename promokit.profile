<?php
/**
 * @file
 * Enables modules and site configuration for installation of PromoKit platform.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function promokit_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = \Drupal::request()->server->get('SERVER_NAME');
  // We have to rename some fields because to keep relationship with our context.
  $form['site_information']['site_name']['#title'] = t('Project name');
//  $form['site_information']['site_mail']['#title'] = t('Project email address');

  // Hide fields that a regular user shouldn't need to see.
  $form['regional_settings']['site_default_country']['#access'] = FALSE;
  $form['regional_settings']['date_default_timezone']['#access'] = FALSE;
  $form['regional_settings']['#access'] = FALSE;
  $form['update_notifications']['update_status_module']['#access'] = FALSE;
  $form['update_notifications']['update_status_module']['#access'] = FALSE;
  $form['update_notifications']['#access'] = FALSE;
}

/**
 * Implements hook_install_tasks_alter().
 */
function promokit_install_tasks_alter(&$tasks, $install_state) {
  // Install in default language.
  unset($tasks['install_select_language']);
}
