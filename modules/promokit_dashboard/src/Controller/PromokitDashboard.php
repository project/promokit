<?php

namespace Drupal\promokit_dashboard\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\core\Url;

class PromokitDashboard extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  function __construct(FormBuilderInterface $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('form_builder'));
  }

  /**
   * Main dashboard page callback.
   */
  public function mainPage($slide_id = 0) {
    if ($slide_id) {
      $slide = Node::load($slide_id);
    }

    if (empty($slide)) {
      $slide = Node::create(array(
        'type'  => 'slide',
        'title' => t('Slide')
      ));
    }
    // Render node form with display 'promokit_dashboard'.
    $node_form = \Drupal::service('entity.form_builder')->getForm($slide, 'promokit_dashboard');

    $dashboard = $this->buildRightDashboard();

    return array(
      'node_form' => array('content' => $node_form, '#prefix' => '<div class="pk-dashboard-main">', '#suffix' => '</div>'),
      'dashboard' => array('content' => $dashboard, '#prefix' => '<div class="pk-dashboard-secondary"><div class="pk-dashboard-secondary-inner">', '#suffix' => '</div></div>'),
    );
  }

  public function buildRightDashboard() {
    $build = array(
      'slides' => $this->formBuilder->getForm('Drupal\promokit_dashboard\Form\DashboardMiniForm'),
    );

    return $build;
  }
}
