; A separate drupal-org-core.make file makes it so we can apply core patches
; if we need to.

api = 2
core = 8.x
projects[drupal][type] = core
projects[drupal][download][type] = git
projects[drupal][download][tag] = 8.0.0-beta4
projects[drupal][download][branch] = 8.0.x
