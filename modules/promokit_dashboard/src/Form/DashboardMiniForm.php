<?php

/**
 * @file
 * Contains \Drupal\menu_ui\Form\MenuLinkEditForm.
 */

namespace Drupal\promokit_dashboard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\menu_link_content\Entity\MenuLinkContent;

class DashboardMiniForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'promokit_dashboard_mini_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_slide_id = \Drupal::routeMatch()->getParameter('slide_id');

    $form['slides'] = array(
      '#type' => 'table',
      '#empty' => $this->t('No slides'),
      '#attributes' => array(
        'id' => 'promokit_dashboard_mini',
      ),
    );

    $slide_nids = \Drupal::entityQuery('node')
      ->condition('type', 'slide')
      ->execute();

    $slides = array();

    /* @var $node \Drupal\node\Entity\Node */
    foreach (entity_load_multiple('node', $slide_nids) as $node) {
      $weight_value = $node->field_weight->value;
      while (isset($slides[$weight_value])) {
        $weight_value++;
      }

      $node->weight = $weight_value;
      $slides[$weight_value] = $node;
    }

    ksort($slides);

    $i = 1;
    foreach ($slides as $node) {
      $nid = $node->id();
      $form['slides'][$nid]['#node'] = $node;
      $title = $node->getTitle() ?: t('Slide');
      if ($nid != $current_slide_id) {
        $title = \Drupal::l($title, Url::fromRoute('promokit.dashboard', array('slide_id' => $nid)));
      }

      $title = $i++ . '. ' . $title;

      $form['slides'][$nid]['node'] = array(
          '#type' => 'markup',
          '#markup' => $title,
        );

      $form['slides'][$nid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 50,
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $node->weight,
        '#attributes' => array(
          'class' => array('node-key'),
        ),
      );

      $form['slides'][$nid]['#attributes']['class'] = array();
      $form['slides'][$nid]['#attributes']['class'][] = 'draggable';
    }

    $form['slides']['#tabledrag'][] = array(
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'node-key',
    );

    $form['actions'] = array('#type' => 'actions', '#weight' => -100, '#tree' => FALSE);

    $form['actions']['new'] = array('#markup' => format_string('<a href="!url" class="button button--primary">+ New slide</a>', array('!url' => Url::fromRoute('promokit.dashboard')->toString())));

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Reorder *'),
      '#suffix' => '<p class="description">* ' . t('Please click the <em>Reorder</em> button to save the order of slides.') . '</p>',
      '#access' => count($slides) > 1,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $slides = $form_state->getValue('slides');

    $current_weight = 1;
    foreach (array_keys($slides) as $nid)  {
      /* @var $node \Drupal\node\NodeInterface */
      $node = $form['slides'][$nid]['#node'];

      $node->field_weight->value = $current_weight++;

      if (empty($node->menu)) {
        // @todo: Find quicker way to load menu definition for node.
        $definition = $this->getMenuDefinition($node);
        if (!empty($definition)) {
          // Set menu item weight according to slide order.
          $definition['weight'] = $node->field_weight->value;

          $node->menu = $definition;
        }
      }

      $node->save();
    }
  }

  /**
   * Load menu definition for the $node.
   *
   * @todo: Find quicker way to load menu definition for node.
   *        It seems like this one is not ideal and heavy.
   *
   * @see menu_ui_node_prepare_form()
   *
   * @param $node
   * @return array|bool
   */
  private function getMenuDefinition($node) {
    /** @var \Drupal\node\NodeTypeInterface $node_type */
    $node_type = $node->type->entity;
    $menu_name = strtok($node_type->getThirdPartySetting('menu_ui', 'parent', 'main:'), ':');

    // Give priority to the default menu
    $type_menus = $node_type->getThirdPartySetting('menu_ui', 'available_menus', array('main'));
    if (in_array($menu_name, $type_menus)) {
      $query = \Drupal::entityQuery('menu_link_content')
        ->condition('route_name', 'entity.node.canonical')
        ->condition('route_parameters', serialize(array('node' => $node->id())))
        ->condition('menu_name', $menu_name)
        ->sort('id', 'ASC')
        ->range(0, 1);
      $result = $query->execute();

      $id = (!empty($result)) ? reset($result) : FALSE;
    }
    // Check all allowed menus if a link does not exist in the default menu.
    if (!$id && !empty($type_menus)) {
      $query = \Drupal::entityQuery('menu_link_content')
        ->condition('route_name', 'entity.node.canonical')
        ->condition('route_parameters', serialize(array('node' => $node->id())))
        ->condition('menu_name', array_values($type_menus), 'IN')
        ->sort('id', 'ASC')
        ->range(0, 1);
      $result = $query->execute();

      $id = (!empty($result)) ? reset($result) : FALSE;
    }
    if ($id) {
      $menu_link = MenuLinkContent::load($id);
      $definition = array(
        'entity_id' => $menu_link->id(),
        'id' => $menu_link->getPluginId(),
        'title' => $menu_link->getTitle(),
        'description' => $menu_link->getDescription(),
        'menu_name' => $menu_link->getMenuName(),
        'parent' => $menu_link->getParentId(),
        'weight' => $menu_link->getWeight(),
      );

      return $definition;
    }

    return FALSE;
  }
}
