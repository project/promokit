PromoKit Landing Page Theme
----------------------------


How to develop:

1. Install Ruby

https://www.ruby-lang.org/en/installation/

2. Install necessary gems:

gem install bootstrap-sass

3. Install necessary node packages (package.json)

npm install

4. Install LiveReload browser plugin

http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions-

5. Start watching for changes (Gruntfile.js)

grunt

Any changes in scss will cause their recompilation.
Any changes either in scss, js or templates will cause page reload.
