<?php

namespace Drupal\promokit_mode_switcher\Controller;


use Drupal\Core\Controller\ControllerBase;
use \Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PromokitModeSwitcher
 * @package Drupal\promokit_mode_switcher\Controller
 */
class PromokitModeSwitcher extends ControllerBase {

  function toggle() {
    $current_mode = \Drupal::config('system.site')->get('page.front');

    $new_mode = ($current_mode == 'presentation') ? 'landing' : 'presentation';

    \Drupal::configFactory()->getEditable('system.site')->set('page.front', $new_mode)->save();

    drupal_set_message(t('The system mode has been switched to !mode', array('!mode' => $new_mode)));

    return new RedirectResponse(\Drupal::url($new_mode));
  }
}
