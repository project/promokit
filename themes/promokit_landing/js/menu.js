/**
 * @file
 * Converts main menu node links to anchors.
 * Enables smooth scroll navigation.
 */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.promokitLandingMenu = {
    attach: function (context, settings) {

      var $a = $(context).find('.navbar-collapse a');
      if ($a.length == 0) {
        return;
      }

      // Convert urls to anchors and enable smooth scroll for the links.
      // Before: <a data-drupal-link-system-path="node/3" href="/node/3">
      // After: <a data-drupal-link-system-path="#node-3" href="#node-3">
      $a.each(function(index, element) {

        var $href = $(element).attr('href');

        // Remove first slash if it exists.
        if ($href.charAt(0) === '/') {
          $href = $href.substr(1);
        }

        // Replace all slashes with dashes.
        $href = $href.replace(new RegExp("\/", 'g'), "-");

        $href = '#' + $href;

        // Replace link attributes.
        $(element).attr('href', $href);
        $(element).attr('data-drupal-link-system-path', $href);

        // Enable smooth scroll.
        $(element).on('click', function(e) {

          e.preventDefault();

          var hash = this.hash;

          // Animate scroll.
          $('html, body').animate({
            scrollTop: $(this.hash).offset().top
          }, 300, function(){
            // Add hash to url.
            window.location.hash = hash;
          });
        });
      });
    }
  };

})(jQuery, Drupal);
